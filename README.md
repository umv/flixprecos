# FlixPreço

> Projeto Flix Preço

# Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```
# Note

Attention when executing the project, activating the allow cors origin.