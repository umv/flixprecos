import Vue from 'vue';
import Router from 'vue-router';

import config from '../config';

import HomePage from '@/components/dash/HomePage'
import LoginPage from '@/components/auth/LoginPage'
import FlixPage from '@/components/auth/FlixPage'

Vue.use(Router);

export const router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'HomePage',
            component: HomePage
        },
        {
            path: '/login',
            name: 'LoginPage',
            component: LoginPage
        },
        {
            path: '/auth/flix',
            name: 'FlixPage',
            component: FlixPage
        },

        // otherwise redirect to home
        {
            path: '*',
            redirect: '/'
        }
    ]
});

router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login', '/auth/flix'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    if(authRequired){

        if(!loggedIn || loggedIn.expires_in < (Date.now() / 1000 | 0)) {

            return next('/login');

            /*if (loggedIn.origin_in === 'flix') {

                this.$store.dispatch('authentication/logout');
                document.location.href = `${config.url.flixdovarejo}`;

            } else {

                return next('/login');

            }*/

        }

    }

    next();

})
