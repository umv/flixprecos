// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import { store } from './_store';
import { router } from './_helpers';
import App from './App'

//import router from './router'
import BootstrapVue from 'bootstrap-vue'
import 'v-slim-dialog/dist/v-slim-dialog.css'
import SlimDialog from 'v-slim-dialog'

// setup fake backend
/*import { configureFakeBackend } from './_helpers';
configureFakeBackend();*/

Vue.use(SlimDialog)
Vue.use(BootstrapVue);
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
